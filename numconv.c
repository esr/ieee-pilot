/******************************************************************************

NAME
  numconv.c -- numeric conversion for PILOT

SYNOPSIS
   int numconv(char *str)	-- function for implicit numeric conversion

DESCRIPTION
   See also nonstd.c and file.c.

LICENSE
  SPDX-License-Identifier: BSD-2-Clause

******************************************************************************/

/*LINTLIBRARY*/
#include "pilot.h"
#include <stdio.h>
#include <stdlib.h>

#include "gencode.h"

int numconv(char *str) {
	/* forced (implicit) numeric-conversion function */
	return (atoi(str));
}

/* numconv.c ends here */

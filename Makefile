#
# Makefile for IEEE PILOT interpreter/compiler
#

# Configuration
#
# Set up your compiler with CC.  It must be ANSI-compliant.
#
# You will need Bison and Flex.
#
# If you have termcap, you can try -DTERMCAP for terminal-independent I/O
# (otherwise the code will assume VT100 compatibility).
#
# PILOTDIR is the directory PILOT will look for its compilation skeleton in.
# You will preobably want to change it.
#
CC       = gcc
SYSTYPE	 = -DTERMCAP
PILOTDIR = .

VERS=$(shell sed -n <NEWS '/^[0-9]/s/:.*//p' | head -1)

YACC = bison
LEX = flex

# Optimization and debugging flags
OPTFLAGS = -g
LDFLAGS  = -s
YFLAGS   = -vt
LFLAGS   = -i

CFLAGS  = $(SYSTYPE) $(OPTFLAGS) -DPILOTDIR=\"$(PILOTDIR)\" -Wno-format-truncation

SOURCES = pilot.h pilot_y.y pilot_l.l gencode.[ch] \
	rpilot.c nonstd.c match.c plib.c numconv.c pilotconv.l
DOCS    = README COPYING NEWS TODO pilot.1 pilotconv.1 comments.adoc tour.adoc
SAMPLES = story.p tea.p speaknum.p goldilocks.p tutor.p
META    = control ieee-pilot.jpg
PILOT   = $(DOCS) Makefile $(SOURCES) $(SAMPLES)

all: pilot pilotconv

pilot: grammar.o lexer.o gencode.o nonstd.o libpilot.a
	$(CC) $(CFLAGS) grammar.o lexer.o gencode.o nonstd.o $(LDFLAGS) libpilot.a $(LEXLIB) -ltermcap -o pilot

libpilot.a: rpilot.o plib.o match.o numconv.o
	ar cr libpilot.a rpilot.o plib.o match.o numconv.o
	-ranlib libpilot.a

grammar.o: grammar.c pilot.h grammar.h gencode.h
lexer.o: lexer.c pilot.h grammar.h gencode.h
gencode.o: gencode.c pilot.h grammar.h gencode.h
nonstd.o: nonstd.c pilot.h grammar.h gencode.h
rpilot.o: rpilot.c pilot.h gencode.h
plib.o: plib.c pilot.h
match.o: match.c pilot.h
numconv.o: numconv.c

lexer.c: pilot_l.l
	$(LEX) $(LFLAGS) -o lexer.c pilot_l.l

grammar.c grammar.h grammar.output: pilot_y.y
	$(YACC) $(YFLAGS) -o grammar.c -d pilot_y.y

pilotconv: pilotconv.l
	$(LEX) $(LFLAGS) -o pilotconv.c pilotconv.l
	$(CC) pilotconv.c -o pilotconv

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

man: pilot.1 pilotconv.1

html: pilot.html pilotconv.html tour.html comments.html

reflow:
	@clang-format --style="{IndentWidth: 8, UseTab: ForIndentation}" -i $$(find . -name "*.[ch]")

GRAMMAR = grammar.c lexer.c grammar.h grammar.output
GENERATED = comments.lp tour.lp pilot.man pilotconv.man pilotconv.c

clean:
	rm -f libpilot.a *.o pilot lexer.c pilotconv grammar.[hc] yacc.*
	rm -f grammar.output pilotconv.c pilotconv *~ \#* *.tar.gz TAGS *.html *.1

uclean: clean
	rm -f $(GENERATED) $(GRAMMAR)

ieee-pilot-$(VERS).tar.gz: $(PILOT) $(GRAMMAR)
	tar --transform='s:^:ieee-pilot-$(VERS)/:' --show-transformed-names -czf ieee-pilot-$(VERS).tar.gz $(PILOT) $(GRAMMAR)

dist: ieee-pilot-$(VERS).tar.gz

TAGS: $(SOURCES)
	etags $(SOURCES)

ieee-pilot-$(VERS).md5: ieee-pilot-$(VERS).tar.gz
	@md5sum ieee-pilot-$(VERS).tar.gz >ieee-pilot-$(VERS).md5

release: ieee-pilot-$(VERS).tar.gz ieee-pilot-$(VERS).md5 pilot.html pilotconv.html
	shipper version=$(VERS) | sh -e -x

refresh: html
	shipper -N -w version=$(VERS) | sh -e -x
